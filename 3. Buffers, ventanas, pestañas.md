# Buffers, ventanas, pestañas

* Buffer: un área de memoria que almacena texto, usualmente de un archivo
    * `:buffers` muestra una lista de los buffers activos
    * `:b3` cambia el buffer actual por el buffer 3
* Ventana: una forma de mostrar un buffer
    * Un buffer puede existir en varias ventanas a la vez
    * `Ctrl-W h` para moverse a la ventana de la izquierda
    * Equivalente para `j` (abajo), `k` (arriba), y `l` (derecha)
* Pestaña: permiten agrupar 1 o más ventanas
    * `Ctrl-PageUp` / `gt` avanza a la siguiente pestaña
    * `Ctrl-PageDown` / `gT` avanza a la pestaña anterior
